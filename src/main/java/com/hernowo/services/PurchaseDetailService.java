package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class PurchaseDetailService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseDetailService.class.getName());

    @Inject
    EntityManager em;

    @Inject
    PurchaseService purchaseService;

    @Inject
    ProductService productService;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try{
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            PurchaseDetail purchaseDetail = om.convertValue(param, PurchaseDetail.class);
            if (purchaseDetail.quantity == null || GeneralConstants.EMPTY_STRING.equals(purchaseDetail.quantity))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(purchaseDetail, customId.get("userId").toString());
            purchaseDetail.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(purchaseDetail.id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            PurchaseDetail purchaseDetail = PurchaseDetail.findById(id);

            if (purchaseDetail == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exist", new String());

            if (req.get("quantity") != null)
                purchaseDetail.quantity = Integer.parseInt(req.get("quantity").toString());

            ObjectActiveAndCreatedDateUtil.updateObject(purchaseDetail, customId.get("userId").toString(), true);
            purchaseDetail.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchaseDetail(id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, quantity from hernowo.sales_detail where true ");


            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());


            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "quantity"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null", "");

            Map<String, Object> result = getPurchaseDetail(id,true);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase detail does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getPurchaseDetail(String id, Boolean withParent) {
        PurchaseDetail purchaseDetail = PurchaseDetail.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (purchaseDetail != null) {
            result.put("id", purchaseDetail.id);
            result.put("quantity", purchaseDetail.quantity);
            if (withParent) {
                Purchase purchase = Purchase.findById(purchaseDetail.purchase == null ? new String() : purchaseDetail.purchase.id);
                result.put("purchase", purchaseService.getPurchase(purchase.id,false));

                Product product = Product.findById(purchaseDetail.product == null ? new String() : purchaseDetail.product.id);
                result.put("product", productService.getProduct(product.id));
            }
        }
        return result;
    }
}
