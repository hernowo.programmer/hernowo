package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@ApplicationScoped
public class CustomerService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class.getName());

    @Inject
    EntityManager em;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try{
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            Customer customer = om.convertValue(param, Customer.class);
            if (customer.name == null || GeneralConstants.EMPTY_STRING.equals(customer.name))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer can't be null or empty", new String());
            if (customer.phone == null || GeneralConstants.EMPTY_STRING.equals(customer.phone))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "phone can't be null or empty", new String());
            if (customer.address == null || GeneralConstants.EMPTY_STRING.equals(customer.address))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "address can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(customer, customId.get("userId").toString());
            customer.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(customer.id));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            Customer customer = Customer.findById(id);

            if (customer == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exist", new String());

            if (req.get("name") != null)
                customer.name = req.get("name").toString();

            if (req.get("phone") != null)
                customer.phone = req.get("phone").toString();

            if (req.get("address") != null)
                customer.address = req.get("address").toString();

            ObjectActiveAndCreatedDateUtil.updateObject(customer, customId.get("userId").toString(), true);
            customer.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getCustomer(customer.id));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});
            boolean filterName = false, filterAddress = false;
            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, name, phone, address from hernowo.customer where true ");

            if(request.get("name") != null) {
                queryString.append(" and \"name\" ilike :paramName ");
                filterName = true;
            }

            if(request.get("address") != null) {
                queryString.append(" and address = :paramAddress ");
                filterAddress = true;
            }

            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());

            if (filterName)
                query.setParameter("paramName", "%" + request.get("name").toString() + "%");

            if (filterAddress)
                query.setParameter("paramAddress", request.get("address").toString());

            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "name",
                            "phone",
                            "address"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "Id can't be null", "");

            Map<String, Object> result = getCustomer(id);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "customer does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getCustomer(String id) {
        Customer customer = Customer.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (customer != null) {
            result.put("id", customer.id);
            result.put("name", customer.name);
            result.put("address", customer.address);
            result.put("phone", customer.phone);
        }
        return result;
    }
}
