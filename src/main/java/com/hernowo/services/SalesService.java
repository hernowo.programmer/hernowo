package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.Customer;
import com.hernowo.models.Sales;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

@ApplicationScoped
public class SalesService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalesService.class.getName());

    @Inject
    EntityManager em;

    @Inject
    CustomerService customerService;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            Sales sales = om.convertValue(param, Sales.class);

            if (sales.customer == null || Customer.findById(sales.customer.id) == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales service not valid", new String());

            if (sales.date == null || GeneralConstants.EMPTY_STRING.equals(sales.date))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date can't be null or empty", new String());

            if (sales.transactionNumber == null || GeneralConstants.EMPTY_STRING.equals(sales.transactionNumber))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "transaction number can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(sales, customId.get("userId").toString());
            sales.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(sales.id,true));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            Sales sales = Sales.findById(id);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            if (sales == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales service does not exist", new String());

            if (req.get("transaction_number") != null)
                sales.transactionNumber = req.get("transaction_number").toString();

            if (req.get("date") != null)
                sales.date = formatter.parse(req.get("date").toString());

            ObjectActiveAndCreatedDateUtil.updateObject(sales, customId.get("userId").toString(), true);
            sales.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSales(sales.id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});
            boolean filterTransactionNumber = false;
            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, date, transaction_number from hernowo.sales where true ");

            if(request.get("name") != null) {
                queryString.append(" and \"transaction_number\" ilike :paramName ");
                filterTransactionNumber = true;
            }

            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());

            if (filterTransactionNumber)
                query.setParameter("paramName", "%" + request.get("name").toString() + "%");

            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "date",
                            "transaction_number"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null", "");

            Map<String, Object> result = getSales(id,true);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales service does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getSales(String id, Boolean withParent) {
        Sales sales = Sales.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (sales != null) {
            result.put("id", sales.id);
            result.put("date", sales.date);
            result.put("transaction_number", sales.transactionNumber);

            if (withParent) {
                Customer customer = Customer.findById(sales.customer == null ? new String() : sales.customer.id);
                result.put("customer", customerService.getCustomer(customer.id));
            }
        }
        return result;
    }
}
