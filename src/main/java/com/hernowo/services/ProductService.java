package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.Customer;
import com.hernowo.models.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class ProductService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class.getName());

    @Inject
    EntityManager em;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try{
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            Product product = om.convertValue(param, Product.class);
            if (product.name == null || GeneralConstants.EMPTY_STRING.equals(product.name))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product can't be null or empty", new String());

            if (product.description == null || GeneralConstants.EMPTY_STRING.equals(product.description))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "description can't be null or empty", new String());
            if (product.price == null || GeneralConstants.EMPTY_STRING.equals(product.price))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "price can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(product, customId.get("userId").toString());
            product.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(product.id));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            Product product = Product.findById(id);

            if (product == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exist", new String());

            if (req.get("name") != null)
                product.name = req.get("name").toString();

            if (req.get("description") != null)
                product.description = req.get("description").toString();

            if (req.get("price") != null)
                product.price = Integer.parseInt(req.get("price").toString());

            ObjectActiveAndCreatedDateUtil.updateObject(product, customId.get("userId").toString(), true);
            product.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getProduct(product.id));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});
            boolean filterName = false;
            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, name, description, price from hernowo.product where true ");

            if(request.get("name") != null) {
                queryString.append(" and \"name\" ilike :paramName ");
                filterName = true;
            }


            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());

            if (filterName)
                query.setParameter("paramName", "%" + request.get("name").toString() + "%");

            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "name",
                            "description",
                            "price"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "Id can't be null", "");

            Map<String, Object> result = getProduct(id);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "product does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getProduct(String id) {
        Product product = Product.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (product != null) {
            result.put("id", product.id);
            result.put("name", product.name);
            result.put("address", product.price);
            result.put("phone", product.description);
        }
        return result;
    }
}
