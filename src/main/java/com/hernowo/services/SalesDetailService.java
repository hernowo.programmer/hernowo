package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.Customer;
import com.hernowo.models.Product;
import com.hernowo.models.Sales;
import com.hernowo.models.SalesDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SalesDetailService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(SalesDetailService.class.getName());

    @Inject
    EntityManager em;

    @Inject
    SalesService salesService;

    @Inject
    ProductService productService;


    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try{
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            SalesDetail salesDetail = om.convertValue(param, SalesDetail.class);
            if (salesDetail.quantity == null || GeneralConstants.EMPTY_STRING.equals(salesDetail.quantity))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "quantity can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(salesDetail, customId.get("userId").toString());
            salesDetail.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(salesDetail.id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            SalesDetail salesDetail = SalesDetail.findById(id);

            if (salesDetail == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exist", new String());

            if (req.get("quantity") != null)
                salesDetail.quantity = Integer.parseInt(req.get("quantity").toString());

            ObjectActiveAndCreatedDateUtil.updateObject(salesDetail, customId.get("userId").toString(), true);
            salesDetail.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getSalesDetail(id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, quantity from hernowo.sales_detail where true ");


            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());


            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "quantity"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null", "");

            Map<String, Object> result = getSalesDetail(id,true);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "sales detail does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getSalesDetail(String id, Boolean withParent) {
        SalesDetail salesDetail = SalesDetail.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (salesDetail != null) {
            result.put("id", salesDetail.id);
            result.put("quantity", salesDetail.quantity);
            if (withParent) {
                Sales sales = Sales.findById(salesDetail.sales == null ? new String() : salesDetail.sales.id);
                result.put("sales", salesService.getSales(sales.id,false));

                Product product = Product.findById(salesDetail.product == null ? new String() : salesDetail.product.id);
                result.put("product", productService.getProduct(product.id));
            }
        }
        return result;
    }
}
