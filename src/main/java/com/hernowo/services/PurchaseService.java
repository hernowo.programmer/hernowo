package com.hernowo.services;

import com.barrans.util.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hernowo.models.Purchase;
import com.hernowo.models.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class PurchaseService implements IAction {
    private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseService.class.getName());

    @Inject
    EntityManager em;

    @Inject
    SupplierService supplierService;

    @Override
    @Transactional
    public SimpleResponse insert(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            Purchase purchase = om.convertValue(param, Purchase.class);

            if (purchase.supplier == null || Supplier.findById(purchase.supplier.id) == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase service not valid", new String());

            if (purchase.date == null || GeneralConstants.EMPTY_STRING.equals(purchase.date))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "date can't be null or empty", new String());

            if (purchase.purchaseNumber == null || GeneralConstants.EMPTY_STRING.equals(purchase.purchaseNumber))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase number can't be null or empty", new String());

            ObjectActiveAndCreatedDateUtil.registerObject(purchase, customId.get("userId").toString());
            purchase.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(purchase.id,true));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    @Transactional
    public SimpleResponse update(Object param, String header) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> req = om.convertValue(param, new TypeReference<>(){});
            Map<String, Object> customId = om.readValue(header, Map.class);

            if (customId.get("userId") == null
                    || customId.get("userId").toString().equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, GeneralConstants.UNAUTHORIZED,
                        new String());

            String id = req.get("id") == null ? GeneralConstants.EMPTY_STRING : req.get("id").toString();

            if (id.equals(GeneralConstants.EMPTY_STRING))
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id is required", new String());

            Purchase purchase = Purchase.findById(id);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            if (purchase == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase service does not exist", new String());

            if (req.get("purchase_number") != null)
                purchase.purchaseNumber = req.get("purchase_number").toString();

            if (req.get("date") != null)
                purchase.date = formatter.parse(req.get("date").toString());

            ObjectActiveAndCreatedDateUtil.updateObject(purchase, customId.get("userId").toString(), true);
            purchase.persist();

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, getPurchase(id,true));

        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();

            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});
            boolean filterPurchaseNumber = false;
            Integer limit = 5, offset = 0;

            if (request.containsKey("limit") && null != request.get("limit"))
                limit = Integer.parseInt(request.get("limit").toString());

            if (request.containsKey("offset") && null != request.get("offset"))
                offset = Integer.parseInt(request.get("offset").toString());

            StringBuilder queryString = new StringBuilder();
            queryString.append("select id, date, purchase_number from hernowo.purchase where true ");

            if(request.get("purchase_number") != null) {
                queryString.append(" and \"purchase_number\" ilike :paramPurchaseNumber ");
                filterPurchaseNumber = true;
            }

            queryString.append(" order by id desc");

            Query query = em.createNativeQuery(queryString.toString());

            if (filterPurchaseNumber)
                query.setParameter("paramPurchaseNumber", "%" + request.get("purchase_number").toString() + "%");

            if (!limit.equals(-99) || !offset.equals(-99)) {
                query.setFirstResult(offset);
                query.setMaxResults(limit);
            }

            List<Object[]> result = query.getResultList();
            List<Map<String,Object>> data =
                    BasicUtils.createListOfMapFromArray(
                            result,
                            "id",
                            "date",
                            "purchase_number"
                    );

            Query qCount = em.createNativeQuery(String.format(queryString.toString()
                    .replaceFirst("select.* from", "select count(*) from ").replaceFirst("order by.*", "")));

            for (Parameter parameter : query.getParameters())
                qCount.setParameter(parameter.getName(), query.getParameterValue(parameter));

            BigInteger count = (BigInteger) qCount.getSingleResult();

            Map<String,Object> response = new HashMap<>();
            response.put("data", data);
            response.put("total", count);
            response.put("filtered", data.size());

            return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, response);

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), "");
        }
    }

    @Override
    public SimpleResponse entity(Object param) {
        try {
            ObjectMapper om = new ObjectMapper();
            Map<String, Object> request = om.convertValue(param, new TypeReference<>(){});

            String id = request.get("id") != null ? request.get("id").toString() : null;

            if (id == null)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "id can't be null", "");

            Map<String, Object> result = getPurchase(id,true);

            if (result.size() == 0)
                return new SimpleResponse(GeneralConstants.VALIDATION_CODE, "purchase service does not exists", new String());
            else
                return new SimpleResponse(GeneralConstants.SUCCESS_CODE, GeneralConstants.SUCCESS, result);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new SimpleResponse(GeneralConstants.FAIL_CODE, e.getMessage(), new String());
        }
    }

    public Map<String, Object> getPurchase(String id, Boolean withParent) {
        Purchase purchase = Purchase.findById(id);
        Map<String,Object> result = new HashMap<>();

        if (purchase != null) {
            result.put("id", purchase.id);
            result.put("date", purchase.date);
            result.put("purchase_number", purchase.purchaseNumber);

            if (withParent) {
                Supplier supplier = Supplier.findById(purchase.supplier == null ? new String() : purchase.supplier.id);
                result.put("supplier", supplierService.getSupplier(supplier.id));
            }
        }
        return result;
    }
}
