package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Entity
@Table(name="product")
public class Product extends CommonObjectActiveAndCreatedDate implements Serializable {

    @Column(name = "name", length = 50, nullable = false)
    @NotNull(message = "Please provide a product name")
    public String name;

    @NotNull(message = "Please provide a product description")
    public String description;

    @NotNull(message = "Please provide a product price")
    public Integer price;
}
