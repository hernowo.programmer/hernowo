package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="sales_detail")
public class SalesDetail extends CommonObjectActiveAndCreatedDate implements Serializable {
    @NotNull(message = "Please provide sales quantity")
    public Integer quantity;

    @ManyToOne
    public Sales sales;

    @ManyToOne
    public Product product;
}
