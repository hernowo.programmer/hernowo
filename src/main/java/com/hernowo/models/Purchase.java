package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;
import com.barrans.util.ObjectActiveAndCreatedDateUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="purchase")
public class Purchase extends CommonObjectActiveAndCreatedDate implements Serializable {
    @Column(name = "purchase_number")
    @NotNull(message = "Please provide a purchase name")
    public String purchaseNumber;
    @NotNull(message = "Please provide a date")
    public Date date;
    @ManyToOne
    public Supplier supplier;
}
