package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="sales")
public class Sales extends CommonObjectActiveAndCreatedDate implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "Please provide a sales date")
    public Date date;

    @Column(name = "transaction_number")
    @NotNull(message = "Please provide transaction number")
    public String transactionNumber;

    @ManyToOne
    public Customer customer;
}
