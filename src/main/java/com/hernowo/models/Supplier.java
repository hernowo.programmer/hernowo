package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="supplier")
public class Supplier extends CommonObjectActiveAndCreatedDate implements Serializable {

    @NotNull(message = "Please provide a name")
    public String name;
    @NotNull(message = "Please provide address")
    public String address;
    @NotNull(message = "Please provide a phone number")
    public String phone;
    @NotNull(message = "Please provide a mobile number")
    public String mobile;
}
