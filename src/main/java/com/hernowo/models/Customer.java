package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="customer")
public class Customer extends CommonObjectActiveAndCreatedDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "name", length = 50, nullable = false)
    @NotNull(message = "Please provide a customer name")
    public String name;

    @NotNull(message = "Please provide a phone number")
    public String phone;

    @NotNull(message = "Please provide an address")
    public String address;
}
