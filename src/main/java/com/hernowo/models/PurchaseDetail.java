package com.hernowo.models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name="purchase_detail")
public class PurchaseDetail extends CommonObjectActiveAndCreatedDate implements Serializable {
    @NotNull(message = "Please provide purchase quantity")
    public Integer quantity;

    @ManyToOne
    public Purchase purchase;

    @ManyToOne
    public Product product;
}
